# Solving the INRC-II Nurse Rostering Problem by Simulated Annealing based on Large Neighborhoods

This repository contains the solutions for the problem presented in the *Second International Nurse Rostering Competition ([INRC-II](http://mobiz.vives.be/inrc2/))*, obtained by the search method described in the paper *Solving the INRC-II Nurse Rostering Problem by Simulated Annealing based on Large Neighborhoods* by Sara Ceschia, Rosita Guido and Andrea Schaerf, Annals of Operations Research, 288:95-113, 2020. The solutions refer to the static version of the problem.

This repository stores also a copy of the datasets used in the experimental analysis of the paper and of the validator.

## Datasets

The competition and extended datasets are stored in folder [`Datasets`](Datasets) in JSON format. 
There are 40 datasets, each one composed by one scenario, three history files and 10 week data files. 
The number of nurses ranges from 30 to 120, while planning horizon from 4 to 32 weeks.


## Results

Results of the static solver in its best parameter configuration.


### 4 weeks instances 

| Instance | Avg     | Best | Dev     |     
| :--------  | ---:|  ---:|   ----: | 
030-4-1-6-2-9-1 | 1693.50 | 1680 | 11.56
030-4-1-6-7-5-3 | 1855.50 | 1835 | 13.22
035-4-0-1-7-1-8 | 1455.00 | 1425 | 17.32
035-4-0-4-2-1-6 | 1663.00 | 1630 | 17.51
035-4-0-5-9-5-6 | 1544.50 | 1530 | 9.26
035-4-0-9-8-7-7 | 1421.50 | 1380 | 24.95
035-4-1-0-6-9-2 | 1391.50 | 1360 | 27.39
035-4-2-8-6-7-1 | 1340.50 | 1315 | 20.20
035-4-2-8-8-7-5 | 1151.00 | 1130 | 13.90
035-4-2-9-2-2-6 | 1577.50 | 1555 | 14.95
035-4-2-9-7-2-2 | 1539.50 | 1510 | 22.54
035-4-2-9-9-2-1 | 1509.00 | 1485 | 15.78
040-4-0-2-0-6-1 | 1626.50 | 1605 | 15.28
040-4-2-6-1-0-6 | 1847.00 | 1805 | 26.69
050-4-0-0-4-8-7 | 1417.50 | 1385 | 19.47
050-4-0-7-2-7-2 | 1404.00 | 1380 | 16.12
060-4-1-6-1-1-5 | 2592.50 | 2550 | 27.61
060-4-1-9-6-3-8 | 2786.00 | 2750 | 20.11
070-4-0-3-6-5-1 | 2455.00 | 2415 | 25.50
070-4-0-4-9-6-7 | 2190.00 | 2155 | 26.67
070-4-0-4-9-7-6 | 2222.00 | 2195 | 15.31
070-4-0-8-6-0-8 | 2345.50 | 2315 | 17.87
070-4-0-9-1-7-5 | 2147.00 | 2115 | 25.63
070-4-1-1-3-8-8 | 2582.50 | 2535 | 19.76
070-4-2-0-5-6-8 | 2365.00 | 2340 | 17.32
070-4-2-3-5-8-2 | 2424.50 | 2400 | 19.50
070-4-2-5-8-2-5 | 2366.50 | 2335 | 19.73
070-4-2-9-5-6-5 | 2416.00 | 2395 | 21.58
080-4-2-4-3-3-3 | 3424.00 | 3390 | 20.92
080-4-2-6-0-4-8 | 3323.00 | 3285 | 28.11
100-4-0-1-1-0-8 | 1315.00 | 1280 | 17.48
100-4-2-0-6-4-6 | 1934.00 | 1885 | 27.16
110-4-0-1-4-2-8 | 2387.50 | 2345 | 23.72
110-4-0-1-9-3-5 | 2566.50 | 2545 | 17.49
110-4-1-0-1-6-4 | 2609.00 | 2570 | 28.36
110-4-1-0-5-8-8 | 2596.00 | 2555 | 32.04
110-4-1-2-9-2-0 | 3032.00 | 2995 | 26.89
110-4-1-4-8-7-2 | 2545.50 | 2495 | 27.02
110-4-2-0-2-7-0 | 2763.50 | 2730 | 16.67
110-4-2-5-1-3-0 | 2719.00 | 2685 | 25.47
110-4-2-8-9-9-2 | 3049.00 | 3010 | 30.89
110-4-2-9-8-4-9 | 2834.00 | 2810 | 21.06
110-4-2-9-8-4-9 | 2835.50 | 2800 | 24.32
120-4-1-4-6-2-6 | 2123.50 | 2065 | 33.42
120-4-1-5-6-9-8 | 2172.50 | 2135 | 18.60

### 8 weeks instances 

| Instance | Avg     | Best | Dev |   
| :--------  | ---:|  ---:|   ----: | 
030-8-1-2-7-0-9-3-6-0-6 | 2098.00 | 2055 | 22.14
030-8-1-6-7-5-3-5-6-2-9 | 1787.00 | 1750 | 23.00
035-8-0-6-2-9-8-7-7-9-8 | 2737.00 | 2705 | 23.12
035-8-1-0-8-1-6-1-7-2-0 | 2527.00 | 2465 | 44.36
040-8-0-0-6-8-9-2-6-6-4 | 2782.00 | 2695 | 58.32
040-8-2-5-0-4-8-7-1-7-2 | 2584.00 | 2525 | 36.12
050-8-1-1-7-8-5-7-4-1-8 | 5002.00 | 4935 | 40.77
050-8-1-9-7-5-3-8-8-3-1 | 4958.50 | 4920 | 34.48
060-8-0-6-2-9-9-0-8-1-3 | 2416.00 | 2375 | 27.16
060-8-2-1-0-3-4-0-3-9-1 | 2675.00 | 2630 | 34.08
070-8-0-3-3-9-2-3-7-5-2 | 4866.00 | 4785 | 55.67
070-8-0-9-3-0-7-2-1-1-0 | 5030.50 | 4950 | 42.65
080-8-1-4-4-9-9-3-6-0-5 | 4477.00 | 4350 | 74.39
080-8-2-0-4-0-9-1-9-6-2 | 4795.00 | 4710 | 64.16
100-8-0-0-1-7-8-9-1-5-4 | 2410.50 | 2340 | 41.93
100-8-1-2-4-7-9-3-9-2-8 | 2557.00 | 2510 | 32.85
110-8-0-2-1-1-7-2-6-4-7 | 4226.00 | 4170 | 41.08
110-8-0-3-2-4-9-4-1-3-7 | 3688.50 | 3650 | 22.98
120-8-0-0-9-9-4-5-1-0-3 | 2871.00 | 2810 | 49.37
120-8-1-7-2-6-4-5-2-0-2 | 3317.00 | 3260 | 37.58

## Solutions

Best solutions are available in the folder [`StaticSolutions`](StaticSolutions). Each solution is validated against the official validator of the competition. 
For each instance, the corresponding folder contains a solution file for each week of the planning horizon and the output of the validator.


## Validator

The solution validator is the original one of the INRC-II competiton, available also from the [competition website](http://mobiz.vives.be/inrc2/) as  [`validator.jar`](validator.jar). 
